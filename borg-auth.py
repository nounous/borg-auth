import argparse
import json
import os

import jinja2
import ldap


path = os.path.dirname(os.path.abspath(__file__))

if __name__ == "__main__":
	parser = argparse.ArgumentParser(description="Generate /var/lib/borg/.ssh/authorized_keys")
	parser.add_argument('-c', '--config', help='Path to the configuration file',
		default=os.path.join(path, 'borg-auth.json'))
	parser.add_argument('-o', '--output', help='File to write authorized_keys to',
		default='/var/lib/borg/.ssh/authorized_keys')
	args = parser.parse_args()

	with open(args.config) as file:
		config = json.load(file)

	hosts = None

	base = ldap.initialize(config['ldap']['server'])
	if config['ldap']['server'].startswith('ldaps://'):
		base.set_option(ldap.OPT_X_TLS_REQUIRE_CERT, ldap.OPT_X_TLS_ALLOW)
		base.set_option(ldap.OPT_X_TLS_NEWCTX, 0)
	hosts =  base.search_s("ou=hosts,dc=crans,dc=org", ldap.SCOPE_SUBTREE, "objectClass=ipHost")

	hosts = [
		{
			key: [ value.decode('utf-8')
				for value in values ] for key,values in host.items() if key in ['cn', 'ipHostNumber']
		} for _,host in hosts if any(cn.decode('utf-8').endswith(config['filter']) for cn in host["cn"])
	]


	hosts_, hosts = hosts, []
	for host in hosts_:
		paths = [
				os.path.join(config['root'], hostname.removesuffix(config['filter']))
			for hostname in host['cn']
		]
		for manual in config['manual']:
			if manual['filter'] in host['ipHostNumber']:
				paths += manual['restrict']
		hosts.append(
			{
				"from": ','.join(host['ipHostNumber']),
				"restrict": ' '.join(f'--restrict-to-path {path}' for path in paths)
			}
		)

	with open(os.path.join(path, 'templates', 'authorized_keys.j2')) as file:
		template = jinja2.Template(file.read())

	with open(args.output, 'w') as file:
		file.write(
			template.render(hosts=hosts, sshkey=config['ssh-key'])
		)
